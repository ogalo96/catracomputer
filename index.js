const http = require('http');
const express = require('express');

const app = express();
const server=http.createServer(app);



app.set('port',process.env.PORT || 3000);


//conigurando archivos estaticos
app.use(express.static('dist'));

//incia servidor
server.listen(app.get('port'),()=>{
    console.log('server en puerto ',app.get('port'));});
