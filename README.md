# catraComputer

## Integrantes
| | |
|---|---|
|Oscar Mauricio de Jesus Galo Duarte   | 20141001126   |
|Gabriel Antonio Gonzalez Vijil   | 20141001297  |  


### URL del proyecto desplegado
https://catracomputer.herokuapp.com/
 


### Ejemplo
```
000     10015       @ pide un numero
001     20015       @ carga AC con el numero ingresado
002     41000       @ verifica si es negativo
003     30014       @ suma
004     21014       @ almacenar el resultado
005     20012       @ cargar # de ciclo
006     31013       @ decrementar el ciclo
007     42010       @ termina el ciclo AC = 0
008     21012       @ almacena decremento
009     40000       @ continua el ciclo
010     11014       @ imprime suma
011     43000       @ fin
012     00010       @ contador del ciclo
013     00001       @ decremento del ciclo
014     00000       @ acumulador suma 
015     00000       @ valor ingresado
```

