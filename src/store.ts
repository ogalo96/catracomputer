import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    ubicacion: '',
  },
  mutations: {
    agregar(state, ubicacion) {
      state.ubicacion = ubicacion;
    },
  },
  actions: {

  },
});
