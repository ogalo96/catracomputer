import Vue from 'vue';
import './plugins/vuetify';
import App from './App.vue';
import router from './router';
import VueCodemirror from 'vue-codemirror';

Vue.config.productionTip = false;

// require styles
import 'codemirror/lib/codemirror.css';

import store from './store';

Vue.config.productionTip = false;
Vue.use(VueCodemirror);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
